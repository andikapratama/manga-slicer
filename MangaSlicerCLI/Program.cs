﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using MangaSlicerLib;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Drawing.Imaging;
using CommandLine.Text;
using CommandLine;

namespace MangaSlicerCLI
{
    class Program
    {

        class Options
        {
            [Option('l', "Compress Level", DefaultValue = "Max",
              HelpText = "How much the compress level to minimize the output file's size.")]
            public string CompressLevel { get; set; }

            [Option('o', "Overwrite the file", DefaultValue = false,
              HelpText = "If true, overwrite the input file with the output")]
            public bool Overwrite { get; set; }

            [HelpOption]
            public string GetUsage()
            {
                return HelpText.AutoBuild(this,
                  (HelpText current) => HelpText.DefaultParsingErrorsHandler(this, current));
            }
        }


        static void Main(string[] args)
        {
            if (args.Length < 1)
            {
                Console.WriteLine("Please state the file or the folder of the images");
                return;
            }

            IEnumerable<FileInfo> files;
            string input = args[0];
            FileAttributes attr = File.GetAttributes(input);

            //detect whether its a directory or file
            if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
            {
                files = getAllFiles(new DirectoryInfo(input));
            }
            else
            {
                files = new List<FileInfo>() { new FileInfo(input) };
            }

            var options = new Options();
            CommandLine.Parser.Default.ParseArguments(args, options);

            CompressionLevel level = CompressionLevel.Max;
            if (!Enum.TryParse<CompressionLevel>(options.CompressLevel, out level))
            {
                Console.WriteLine(string.Format("Unrecognized compression level, The default would be assumed ({0}).", level));
            }

            var slicer = new BlankSlicer();
            var regexImageFile = new Regex("png|jpe?g|bmp|gif");
            ICompressor compressor = new PNGOutCompressor();
            foreach (var file in files)
            {
                if (!regexImageFile.IsMatch(file.Extension.Trim()))
                {
                    continue;
                }
                try
                {
                    Console.Write("Processing {0}..", file.Name);
                    var image = new Bitmap(file.FullName);
                    var slicedImage = slicer.Slice(image);
                    var outputImagePath = compressor.Compress(slicedImage, level);
                    if (options.Overwrite)
                    {
                        file.Delete();
                        MoveWithReplace(outputImagePath, file.FullName);
                    }
                    else
                    {
                        MoveWithReplace(outputImagePath, Path.Combine(file.DirectoryName, "sliced-" + file.Name));
                    }
                    Console.WriteLine("   Finished!");
                }
                catch (Exception e)
                {
                    Console.WriteLine("  Unexpected Error: " + e.Message);
                }
            }
            Console.WriteLine("Done! press any key to continue..");
            Console.ReadKey();

        }

        static IEnumerable<FileInfo> getAllFiles(DirectoryInfo di)
        {
            List<FileInfo> files = di.GetFiles().ToList();
            foreach (var dir in di.GetDirectories())
            {
                files.AddRange(getAllFiles(dir));
            }
            return files;
        }

        public static void MoveWithReplace(string sourceFileName, string destFileName)
        {

            //first, delete target file if exists, as File.Move() does not support overwrite
            if (File.Exists(destFileName))
            {
                File.Delete(destFileName);
            }

            File.Move(sourceFileName, destFileName);

        }

    }
}
