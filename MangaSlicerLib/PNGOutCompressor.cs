﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;
using System.Reflection;
using System.ComponentModel;

namespace MangaSlicerLib
{
    public class PNGOutCompressor : ICompressor
    {
        public sealed class PNGOutStrategyOptions
        {
            private readonly String name;
            private readonly String displayName;
            private readonly int value;

            public static readonly PNGOutStrategyOptions Extreme = new PNGOutStrategyOptions(1, "/s0", "Extreme");
            public static readonly PNGOutStrategyOptions Intense = new PNGOutStrategyOptions(2, "/s1", "Intense");
            public static readonly PNGOutStrategyOptions Fast = new PNGOutStrategyOptions(3, "/s2", "Fast");
            public static readonly PNGOutStrategyOptions Faster = new PNGOutStrategyOptions(3, "/s3", "Faster");
            public static readonly PNGOutStrategyOptions Fastest = new PNGOutStrategyOptions(3, "/s4", "Fastest");

            static PNGOutStrategyOptions()
            {
                var type = typeof(PNGOutStrategyOptions);
                var fields = type.GetFields();
                parseDict = new Dictionary<string, PNGOutStrategyOptions>();
                foreach (var field in fields)
                {
                    if (field.IsStatic && field.IsPublic && field.IsInitOnly && field.FieldType == type)
                    {
                        parseDict.Add(field.Name, (PNGOutStrategyOptions)field.GetValue(null));
                    }
                }
            }

            private static readonly Dictionary<string, PNGOutStrategyOptions> parseDict;

            private PNGOutStrategyOptions(int value, String name, string displayName)
            {
                this.name = name;
                this.value = value;
                this.displayName = displayName;
            }
            public static PNGOutStrategyOptions Parse(string s)
            {
                return parseDict[s];
            }

            public string Name
            {
                get { return displayName; }
            }

            public override String ToString()
            {
                return name;
            }
        }

        string pngoutFilename;

        int Progress { get; set; }

        public PNGOutCompressor()
        {
            string tempPath = System.IO.Path.GetTempPath();
            pngoutFilename = Path.Combine(tempPath, Guid.NewGuid().ToString() + ".exe");
            File.WriteAllBytes(pngoutFilename, MangaSlicerLib.Properties.Resources.pngout);

        }

        ~PNGOutCompressor()
        {
            File.Delete(pngoutFilename);
        }

        public string Compress(Bitmap image, CompressionLevel level = CompressionLevel.Medium)
        {
            PNGOutStrategyOptions so;
            switch (level)
            {
                case CompressionLevel.Fast:
                    so = PNGOutStrategyOptions.Fastest;
                    break;
                case CompressionLevel.Max:
                    so = PNGOutStrategyOptions.Extreme;
                    break;
                default:
                    so = PNGOutStrategyOptions.Intense;
                    break;
            }

            string tempPath = System.IO.Path.GetTempPath();
            string path = Path.Combine(tempPath, Guid.NewGuid().ToString());
            image.Save(path);
            string optimizedPath = Path.Combine(tempPath, Guid.NewGuid().ToString()+".png");
            ProcessStartInfo psi = new ProcessStartInfo(pngoutFilename, string.Format("{0} {1} {2} /c3", path, optimizedPath, so));
            psi.RedirectStandardOutput = true;
            psi.UseShellExecute = false;
            psi.WindowStyle = ProcessWindowStyle.Hidden;
            psi.CreateNoWindow = true;
            Process p = Process.Start(psi);
            p.BeginOutputReadLine();
            p.WaitForExit();
            File.Delete(path);
            return optimizedPath;
        }

        public Task<string> CompressAsync(Bitmap image, CompressionLevel level = CompressionLevel.Medium)
        {
            return Task.Run<string>(() =>
            {
                return Compress(image, level);
            });
        }
    }
}
