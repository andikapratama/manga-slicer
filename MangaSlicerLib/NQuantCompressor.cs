﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using nQuant;
using System.IO;
using System.Drawing.Imaging;

namespace MangaSlicerLib
{
    public class NQuantCompressor : ICompressor
    {
        WuQuantizer quantizer = new WuQuantizer();

        public string Compress(System.Drawing.Bitmap image, CompressionLevel level)
        {
            string tempPath = System.IO.Path.GetTempPath();
            string path = Path.Combine(tempPath, Guid.NewGuid().ToString())+".png";
            using (var quantized = quantizer.QuantizeImage(image))
            {
            
                quantized.Save(path, ImageFormat.Png);
            }
            return path;
        }

        public Task<string> CompressAsync(System.Drawing.Bitmap image, CompressionLevel level)
        {
            return Task.Run<string>(() =>
            {
                return Compress(image, level);
            });
        }
    }
}
