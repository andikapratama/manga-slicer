﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;

namespace MangaSlicerLib
{
    public enum CompressionLevel
    {
        Fast, Medium, Max
    }

    public interface ICompressor
    {
        string Compress(Bitmap image, CompressionLevel level = CompressionLevel.Medium);
        Task<string> CompressAsync(Bitmap image, CompressionLevel level = CompressionLevel.Medium); 
    }
}
