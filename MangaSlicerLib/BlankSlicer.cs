﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;
using AForge.Imaging;
using Line = System.Tuple<int,int>;
using System.Diagnostics;

namespace MangaSlicerLib
{
    /// <summary>
    /// Naive slicer, this only cuts extraneous white margins & gaps in a page
    /// </summary>
    public class BlankSlicer
    {

        public Bitmap Slice(Bitmap bmp, int tolerance =105, int gap = 1, double widthIgnoreRatio = 0.1)
        {
            var imagesHorizontal = new ConcurrentQueue<Line>();
            var imagesVertical = new ConcurrentQueue<Line>();

            int widthIgnore = (int)((bmp.Height >= bmp.Width ? bmp.Width : bmp.Height) * widthIgnoreRatio);
            
            //get the images rectangle blobs, width first, then height
            Rectangle rect = new Rectangle(0, 0, bmp.Width, bmp.Height);
            System.Drawing.Imaging.BitmapData bmpData =
                bmp.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadOnly,
                bmp.PixelFormat);
            UnmanagedImage uimage = new UnmanagedImage(bmpData);

            var dimensions = new List<Tuple<int, int, ConcurrentQueue<Line>, bool, UnmanagedImage>>() 
            {
                Tuple.Create(bmp.Width,bmp.Height,imagesVertical,true,uimage),
                Tuple.Create(bmp.Height,bmp.Width,imagesHorizontal,false,uimage)
            };
            
            Color[] palette = bmp.Palette.Entries;

            ///this should be equal to the iterative version but run parallel. However it doesn't work! left for further research later.
            //Parallel.ForEach(dimensions, dimension =>
            //{
            //    int startImage = -1;
            //    for (int x = 0; x < dimension.Item1; x++)
            //    {
            //        bool isFree = true;
            //        for (int y = 0; y < dimension.Item2; y++)
            //        {
            //            Color c = dimension.Item4 ? dimension.Item5.GetPixel(x, y) : dimension.Item5.GetPixel(y, x);
            //            if (bmp.PixelFormat == System.Drawing.Imaging.PixelFormat.Format8bppIndexed)
            //            {
            //                if (palette[c.R].R < tolerance)
            //                {
            //                    isFree = false;
            //                    break;
            //                }
            //            }
            //            else if (c.R < tolerance || c.G < tolerance || c.B < tolerance)
            //            {
            //                isFree = false;
            //                break;
            //            }
            //        }
            //        if (!isFree && startImage < 0)
            //        {
            //            startImage = x;
            //        }
            //        else if (isFree && startImage >= 0)
            //        {
            //            dimension.Item3.Enqueue(Tuple.Create(startImage, x));
            //            startImage = -1;
            //        }
            //    }
            //    if (startImage >= 0)
            //    {
            //        dimension.Item3.Enqueue(Tuple.Create(startImage, dimension.Item1 - 1));
            //    }
            //});

            foreach (var dimension in dimensions)
            {
                int startImage = -1;
                for (int x = 0; x < dimension.Item1; x++)
                {
                    bool isFree = true;
                    for (int y = 0; y < dimension.Item2; y++)
                    {
                        Color c = dimension.Item4 ? dimension.Item5.GetPixel(x, y) : dimension.Item5.GetPixel(y, x);
                        if (bmp.PixelFormat == System.Drawing.Imaging.PixelFormat.Format8bppIndexed)
                        {
                            if (palette[c.R].R < tolerance)
                            {
                                isFree = false;
                                break;
                            }
                        }
                        else if (c.R < tolerance || c.G < tolerance || c.B < tolerance)
                        {
                            isFree = false;
                            break;
                        }
                    }
                    if (!isFree && startImage < 0)
                    {
                        startImage = x;
                    }
                    else if (isFree && startImage >= 0)
                    {
                        dimension.Item3.Enqueue(Tuple.Create(startImage, x));
                        startImage = -1;
                    }
                }
                if (startImage >= 0)
                {
                    dimension.Item3.Enqueue(Tuple.Create(startImage, dimension.Item1 - 1));
                }
            }; 
            bmp.UnlockBits(bmpData);
            var limagesHorizontal = imagesHorizontal.Where(x => (x.Item2 - x.Item1) > widthIgnore).ToList();
            var limagesVertical = imagesVertical.Where(x => (x.Item2 - x.Item1) > widthIgnore).ToList();
            return reduceHorizontal(reduceVertical(bmp, limagesVertical, gap), limagesHorizontal, gap);
        }

        private Bitmap reduceVertical(Bitmap bmp, List<Line> imagesVertical, int Gap = 0)
        {
            if (imagesVertical.Count == 0)
            {
                return bmp;
            }
            int finalWidth = 0;
            foreach (var image in imagesVertical)
            {
                finalWidth += (image.Item2 - image.Item1);
            }
            finalWidth += (imagesVertical.Count - 1) * Gap;
            var images = imagesVertical.Select(tup => new Rectangle(tup.Item1, 0, (tup.Item2 - tup.Item1), bmp.Height)).ToList();

            Bitmap newBmp = new Bitmap(finalWidth, bmp.Height);
            using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(newBmp))
            {
                //set background color
                g.Clear(System.Drawing.Color.White);
                int offset = 0;
                for (int i = 0; i < images.Count; i++)
                {
                    var imageRect = images[i];
                    g.DrawImage(bmp, new Rectangle(offset, 0, imageRect.Width, imageRect.Height), imageRect, GraphicsUnit.Pixel);
                    offset += imageRect.Width + Gap;
                }
            }

            return newBmp;
        }

        private Bitmap reduceHorizontal(Bitmap bmp, List<Line> imagesHorizontal, int Gap = 0)
        {
            if (imagesHorizontal.Count == 0)
            {
                return bmp;
            }

            int finalHeight = 0;
            foreach (var image in imagesHorizontal)
            {
                finalHeight += (image.Item2 - image.Item1);
            }
            finalHeight += (imagesHorizontal.Count - 1) * Gap;
            var images = imagesHorizontal.Select(tup => new Rectangle(0, tup.Item1, bmp.Width, (tup.Item2 - tup.Item1))).ToList();

            Bitmap newBmp = new Bitmap(bmp.Width, finalHeight);
            using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(newBmp))
            {
                //set background color
                g.Clear(System.Drawing.Color.White);
                int offset = 0;
                for (int i = 0; i < images.Count; i++)
                {
                    var imageRect = images[i];
                    g.DrawImage(bmp, new Rectangle(0, offset, imageRect.Width, imageRect.Height), imageRect, GraphicsUnit.Pixel);
                    offset += imageRect.Height + Gap;
                    
                }

                return newBmp;
            }
        }
    }
}
