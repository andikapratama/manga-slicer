﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MangaSlicerLib
{
    public class OptiPNGCompressor : ICompressor
    {

        string optipngFilename;



        public OptiPNGCompressor()
        {
            string tempPath = System.IO.Path.GetTempPath();
            optipngFilename = Path.Combine(tempPath, Guid.NewGuid().ToString() + ".exe");
            File.WriteAllBytes(optipngFilename, MangaSlicerLib.Properties.Resources.pngout);

        }

        ~OptiPNGCompressor()
        {
            File.Delete(optipngFilename);
        }

        public string Compress(System.Drawing.Bitmap image, CompressionLevel level)
        {
            string tempPath = System.IO.Path.GetTempPath();
            string path = Path.Combine(tempPath, Guid.NewGuid().ToString() + ".png");
            image.Save(path, ImageFormat.Png);
            int optLevel;
            switch (level)
            {
                case CompressionLevel.Fast:
                    optLevel = 1;
                    break;
                case CompressionLevel.Medium:
                    optLevel = 2;
                    break;
                default:
                    optLevel = 7;
                    break;

            }
            ProcessStartInfo psi = new ProcessStartInfo(optipngFilename, string.Format("-o{0} {1}",optLevel, path));
            psi.RedirectStandardOutput = true;
            psi.UseShellExecute = false;
            psi.WindowStyle = ProcessWindowStyle.Hidden;
            psi.CreateNoWindow = true;
            Process p = Process.Start(psi);
            p.BeginOutputReadLine();
            p.WaitForExit();
            return path;
        }

        public Task<string> CompressAsync(System.Drawing.Bitmap image, CompressionLevel level)
        {
            return Task.Run<string>(() =>
            {
                return Compress(image, level);
            });
        }
    }
}
