﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using MangaSlicerLib;

namespace MangaSlicer
{
    public partial class PreferencesForm : Form
    {
        public PreferencesForm()
        {
            InitializeComponent();
        }

        private void PreferencesForm_Load(object sender, EventArgs e)
        {
            comboBox1.Text = Preferences.current.compressionStrategy.ToString();
            numericUpDown1.Text = Preferences.current.tolerance.ToString();
            numericUpDown2.Text = Preferences.current.pixelGap.ToString();
            numericUpDown3.Text = Preferences.current.narrowGapTolerance.ToString();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            Preferences.current.compressionStrategy = (CompressionLevel)Enum.Parse(typeof(CompressionLevel), comboBox1.Text);
            Preferences.current.tolerance = int.Parse(numericUpDown1.Text);
            Preferences.current.pixelGap = int.Parse(numericUpDown2.Text);
            Preferences.current.narrowGapTolerance = int.Parse(numericUpDown3.Text);

            File.WriteAllText(Util.homePath,
            JsonConvert.SerializeObject(Preferences.current.ToJSON(), new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All
            }));
            this.Hide();
        }
    }
}
