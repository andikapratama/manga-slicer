﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using MangaSlicerLib;

namespace MangaSlicer
{
    class Preferences
    {

        private static void savePreferences()
        {
            string jsonized = JsonConvert.SerializeObject(Preferences._current, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All,
                Formatting = Formatting.Indented
            });
            File.WriteAllText("MangaSlicer.preferences", jsonized);
        }

        public static Preferences current
        {
            get
            {
                if (_current == null)
                {
                    _current = new Preferences();
                    _current.tolerance = 101;
                    _current.narrowGapTolerance = 0.05;
                    _current.pixelGap = 1;
                    _current.compressionStrategy = CompressionLevel.Max;
                    _current.outputDirectory = Util.homePath;
                }
                return _current;
            }
            set
            {
                _current = value;
            }
        }
        private static Preferences _current;

        public CompressionLevel compressionStrategy { get; set; }
        public string outputDirectory { get; set; }
        public int tolerance { get; set; }
        public int pixelGap { get; set; }
        public double narrowGapTolerance { get; set; }

        /// <summary>
        /// simple class to hold data when saving & loading
        /// </summary>
        public class PreferencesJSON
        {
            public string compressionStrategy { get; set; }
            public string outputDirectory { get; set; }
            public int tolerance { get; set; }
            public int pixelGap { get; set; }
            public double narrowGapTolerance { get; set; }
        }

        public void SetFromJSON(PreferencesJSON json) 
        {
            this.narrowGapTolerance = json.narrowGapTolerance;
            this.outputDirectory = json.outputDirectory;
            this.pixelGap = json.pixelGap;
            this.tolerance = json.tolerance;
            //this.compressionStrategy = Compressor.PNGOutStrategyOptions.Parse(json.compressionStrategy);
        }

        public PreferencesJSON ToJSON() 
        {
            var json = new PreferencesJSON();
            json.narrowGapTolerance = this.narrowGapTolerance;
            json.outputDirectory = this.outputDirectory;
            json.pixelGap = this.pixelGap;
            json.tolerance = this.tolerance;
            //json.compressionStrategy = this.compressionStrategy.Name;
            return json;
        }
    }
}
