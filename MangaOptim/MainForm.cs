﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Imaging;
using Newtonsoft.Json;
using MangaSlicerLib;

namespace MangaSlicer
{
    public partial class MainForm : Form
    {
        PreferencesForm preferencesForm;

        public MainForm()
        {
            InitializeComponent();


        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var preferencesFile = (Preferences.PreferencesJSON)JsonConvert.DeserializeObject(File.ReadAllText(Util.homePath), new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All
            });

            Preferences.current.SetFromJSON(preferencesFile);
            
        }

        void mst_Finished(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void browseButton_Click(object sender, EventArgs e)
        {

        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {

        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void toolsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (preferencesForm == null ||
                preferencesForm.IsDisposed) 
            {
                preferencesForm = new PreferencesForm();
            }
            preferencesForm.Show();
        }

        private void MainForm_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Move;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void MainForm_DragDrop(object sender, DragEventArgs e)
        {
            var paths = (string[])e.Data.GetData(DataFormats.FileDrop);
            var files = paths.SelectMany(path =>
            {
                if (Directory.Exists(path))
                {
                    return Util.getAllFiles(new DirectoryInfo(path));
                }
                else
                {
                    return new List<FileInfo>() { new FileInfo(path) };
                }
            });

            foreach(var file in files)
            {
            }
        }
    }
}
