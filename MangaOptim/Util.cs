﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using MangaSlicerLib;

namespace MangaSlicer
{
    class Util
    {
        public static string homePath { 
            get {

            var homePath = (Environment.OSVersion.Platform == PlatformID.Unix ||
       Environment.OSVersion.Platform == PlatformID.MacOSX)
        ? Environment.GetEnvironmentVariable("HOME")
         : Environment.ExpandEnvironmentVariables("%HOMEDRIVE%%HOMEPATH%");
            var savedPreferencesPath = Path.Combine(homePath, "MangaSlicer.preferences");
            return savedPreferencesPath;
        } }

        public static IList<FileInfo> getAllFiles(DirectoryInfo di)
        {
            List<FileInfo> files = di.GetFiles().ToList();
            foreach (var dir in di.GetDirectories())
            {
                files.AddRange(getAllFiles(dir));
            }
            return files;
        }
    }
}
